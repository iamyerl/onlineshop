
import '@/assets/css/style.css';
// import '@/assets/css/bootstrap.min.css';
// import '@/assets/css/font-awesome.min.css';
import '@/assets/css/nouislider.min.css';
import '@/assets/css/slick.css';
// import '@/assets/css/slick-theme.css';
import '@/assets/css/style.css';

// import '@/assets/js/main.js'
// import '@/assets/js/nouislider.min.js'
// import '@/assets/js/slick.min.js'
// import '@/assets/js/jquery.zoom.min.js'
// import '@/assets/js/jquery.min.js'
// import '@/assets/js/bootstrap.min.js'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

createApp(App).use(router).mount('#app')
